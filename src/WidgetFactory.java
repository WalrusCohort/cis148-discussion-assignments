// name : Matt Perna
// date : 17 Oct, 2018
// This application will demonstrate the creation of a collection of objects.
// There will be user input to create individual widget objects that contain a name field and a price
// Team member with branch:

public class WidgetFactory {
    public static void main(String[] args) {
        //main method goes here
        /*
            - Setup a Scanner object to read user inputs
            - Use a loop to
                - prompt the user to input a widget name and price
                - store values in a new widget
                - add the widget to a widget array
                - update the widget count
            - You can ONLY create up to the maximum number of widgets allowed
            - Display the collection of widgets and the average price
         */
    }
}

// name : Matt Perna
// date : 17 Oct, 2018
// This application will demonstrate the creation of a collection of objects.
// There will be user input to create individual widget objects that contain a name field and a price
// Team member with branch:

public class Widget {
    // object fields (object variable, members, etc.)
    private String name;
    private double price;
    //Class fields (class variable that are created even without any objects created)
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    //Argument based Constructor
    public Widget(String name, double price) {

    }
    //No argument constructor
    public Widget() {
        this(name:"Generic", price:1.0);
    }
    //Object methods
    public String getName() {return name; }
    public double getPrice() {return price; }
    public void setPrice() {}

    ///Class methods
    public static int getMaxWidgets() {return MAX_WIDGETS; }
    public static int getCount() { return count; }
    public static void updateCount() { count++;}

}
